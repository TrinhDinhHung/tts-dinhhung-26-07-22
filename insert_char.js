const effect_charater = function(html_charater, char, timeLate) {
  this.html_charater = html_charater;
  this.char = char;
  this.n_char = '';
  this.index_char = 0;
  this.timeLate = parseInt(timeLate, 10);
  this.fun_char();
  this.deleting_char = false;
}
document.addEventListener('DOMContentLoaded', run_Func);
function run_Func() {
  const html_charater = document.querySelector('.write_Char');
  const char = JSON.parse(html_charater.getAttribute('data-char'));
  const timeLate = html_charater.getAttribute('data-timeLate');
  new effect_charater(html_charater, char, timeLate);
}

effect_charater.prototype.fun_char = function() {
  const index_char_now = this.index_char % this.char.length;
  const char_now = this.char[index_char_now];
  if(this.deleting_char) {
    this.n_char = char_now.substring(0, this.n_char.length - 1);
  } else {
    this.n_char = char_now.substring(0, this.n_char.length + 1);
  }
  this.html_charater.innerHTML = `<span class="n_char">${this.n_char}</span>`
  let speed_write_Char = 300;
  if(this.deleting_char) {
    speed_write_Char = speed_write_Char /2;
  }
  if(!this.deleting_char && this.n_char === char_now) {
    speed_write_Char = this.timeLate;
    console.log(this.timeLate);
    this.deleting_char = true;
  } else if (this.deleting_char && this.n_char === ''){
    this.deleting_char = false;
    this.index_char++;
    speed_write_Char = 300;
  }
  setTimeout(() => this.fun_char(), speed_write_Char);
}

